﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Aspirantes.Models
{
    [Table("dbo.Aspirantes")]
    public class Aspirante
    {
        [Key]
        public int Id { get; set; }
        [Required,Display(Name ="Nombre(s)")]
        public string Nombre { get; set; }
        [Required, Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }
        [Required, Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }
        [Required, Display(Name = "CURP")]
        public string CURP { get; set; }
        [Required, Display(Name = "Escuela antierio")]
        public string EscuelaAnterior { get; set; }    
        [Required, Display(Name = "Fecha de nacimiento")]
        public DateTime FechaNacimiento { get; set; }



    }
}
