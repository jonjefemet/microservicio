﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Aspirantes.Data;
using Aspirantes.Models;

namespace Aspirantes.API
{
    [Produces("application/json")]
    [Route("api/AspirantesAPI")]
    public class AspirantesAPI : Controller
    {
        private readonly ApplicationDbContext _context;

        public AspirantesAPI(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/AspirantesAPI
        [HttpGet]
        public IEnumerable<Aspirante> GetAspirantes()
        {
            return _context.Aspirantes;
        }

        // GET: api/AspirantesAPI/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAspirante([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aspirante = await _context.Aspirantes.SingleOrDefaultAsync(m => m.Id == id);

            if (aspirante == null)
            {
                return NotFound();
            }

            return Ok(aspirante);
        }

        // PUT: api/AspirantesAPI/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAspirante([FromRoute] int id, [FromBody] Aspirante aspirante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aspirante.Id)
            {
                return BadRequest();
            }

            _context.Entry(aspirante).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AspiranteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AspirantesAPI
        [HttpPost]
        public async Task<IActionResult> PostAspirante([FromBody] Aspirante aspirante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Aspirantes.Add(aspirante);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAspirante", new { id = aspirante.Id }, aspirante);
        }

        // DELETE: api/AspirantesAPI/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAspirante([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aspirante = await _context.Aspirantes.SingleOrDefaultAsync(m => m.Id == id);
            if (aspirante == null)
            {
                return NotFound();
            }

            _context.Aspirantes.Remove(aspirante);
            await _context.SaveChangesAsync();

            return Ok(aspirante);
        }

        private bool AspiranteExists(int id)
        {
            return _context.Aspirantes.Any(e => e.Id == id);
        }
    }
}